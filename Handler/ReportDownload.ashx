﻿<%@ WebHandler Language="C#" Class="ReportDownload.Downloader" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ReportDownload
{
    /// <summary>
    /// Summary description for ImageViewer
    /// </summary>
    public class Downloader : IHttpHandler
    {
        private string contentType = string.Empty;
        private string fileName = string.Empty;
        Stream stream = null;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();

            if (!String.IsNullOrEmpty(context.Request.QueryString[0]))
            {
                string id = (context.Request.QueryString[0]).Split(',')[0];
                
                
                byte[] reportFile = GetReport(new Guid(id));
                context.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                context.Response.ContentType = contentType;
                context.Response.OutputStream.Write(reportFile,0,reportFile.Length);
                context.Response.Flush();
            }
            else
            {
                context.Response.ContentType = "text/html";
                context.Response.Write("<p>Need a valid id</p>");
            }  
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private byte[] GetReport(Guid ID)
        {
            MemoryStream memoryStream = new MemoryStream();
            memoryStream = null;
            SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder()
                {
                    UserID = "sa",
                    Password = "i3",
                    DataSource = "psodata1",
                    InitialCatalog = "ReportStorage"
                };
            using (var con = new SqlConnection(connectionString.ConnectionString))
            {
                con.Open();
                using(var cmd = new SqlCommand("getReportFile", con){CommandType = CommandType.StoredProcedure})
                {
                    cmd.Parameters.AddWithValue("@Fileid", ID);
                    using (var rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {

                            // object obj = dc.ProductImageSelect(ID);
                            byte[] file = (byte[]) rdr[1];
                            memoryStream = new MemoryStream(file, false);
                            contentType = rdr.GetString(2);
                            fileName = rdr.GetString(3);
                        }
                    }
                }
            }
            return memoryStream.ToArray();
        }
    }
}