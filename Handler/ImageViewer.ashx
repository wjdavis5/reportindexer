﻿<%@ WebHandler Language="C#" Class="ASPNETImage.ImageViewer" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ASPNETImage
{
    /// <summary>
    /// Summary description for ImageViewer
    /// </summary>
    public class ImageViewer : IHttpHandler
    {
        Stream stream = null;
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.Clear();

                if (!String.IsNullOrEmpty(context.Request.QueryString[0]))
                {
                    string id = (context.Request.QueryString[0]).Split(',')[1];
                    context.Response.ContentType = "image/jpeg";
                    //int buffersize = 1024 * 16;
                    //byte[] buffer = new byte[buffersize];

                    Image img = GetImage(new Guid(id));
                    if ((context.Request.QueryString[0]).Split(',')[0] == "Thumb")
                        img.GetThumbnailImage(100, 100, null, IntPtr.Zero)
                           .Save(context.Response.OutputStream, ImageFormat.Jpeg);
                    else
                        img.Save(context.Response.OutputStream, ImageFormat.Jpeg);
                    //int count = stream.Read(buffer, 0, buffersize);
                    //while (count > 0)
                    //{
                    //    context.Response.OutputStream.Write(buffer, 0, count);
                    //    count = stream.Read(buffer, 0, buffersize); 
                    //}
                }
                else
                {
                    context.Response.ContentType = "text/html";
                    context.Response.Write("<p>Need a valid id</p>");
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private Image GetImage(Guid ID)
        {
            MemoryStream memoryStream = new MemoryStream();
            memoryStream = null;
            SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder()
                {
                    UserID = "sa",
                    Password = "i3",
                    DataSource = "psodata1",
                    InitialCatalog = "ReportStorage"
                };
            using (var con = new SqlConnection(connectionString.ConnectionString))
            {
                con.Open();
                using(var cmd = new SqlCommand("getScreenShot", con){CommandType = CommandType.StoredProcedure})
                {
                    cmd.Parameters.AddWithValue("@ScreenShotId", ID);
                    using (var rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {

                            // object obj = dc.ProductImageSelect(ID);
                            byte[] img = (byte[]) rdr[0];
                            memoryStream = new MemoryStream(img, false);
                        }
                    }
                }
            }
            if (memoryStream == null) return null;
            return Image.FromStream(memoryStream);
        }
    }
}