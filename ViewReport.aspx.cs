﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
public partial class ViewReport : System.Web.UI.Page
{
    public string reportID = string.Empty;
    public string fileID = string.Empty;
    public Report _report;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var qString = Request.QueryString[0];
            if (qString.Split(',').Count() > 1)
            {
                reportID = qString.Split(',')[0];
                fileID = qString.Split(',')[1];
            }
            else
            {
                reportID = qString.Split(',')[0];
                fileID = "noFile";
            }
            if (string.IsNullOrEmpty(reportID)) Response.Redirect("Default.aspx");
            _report = Report.GetDataBaseReport(reportID);
            if (_report == null) Response.Redirect("Default.aspx");
        }
        catch (ArgumentOutOfRangeException)
        {
            Response.Redirect("Default.aspx");
        }
    }
    protected void btnSubmitTag_Click(object sender, EventArgs e)
    {
        var tag = txtTagName.Text;
        Report.AddTag(reportID,tag);
        Response.Redirect("ViewReport.aspx?"+reportID +"&"+fileID);

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        
        
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        if (!fileReport.HasFile) return;
        var filename = Path.GetFileName(fileReport.FileName);
        if (Path.GetExtension(filename).ToLower() != ".jpg"
            && Path.GetExtension(filename).ToLower() != ".png"
            && Path.GetExtension(filename).ToLower() != ".gif"
            && Path.GetExtension(filename).ToLower() != ".jpeg") return;

        fileReport.SaveAs(Server.MapPath("~/") + filename);
        var buffer = new byte[fileReport.PostedFile.InputStream.Length];
        using (var ms = new MemoryStream())
        {
            int read;
            while ((read = fileReport.PostedFile.InputStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            Report.AddScreenShot(reportID, buffer);
        }
        Response.Redirect("ViewReport.aspx?" + reportID + "&" + fileID);

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Button1_Click1(sender,e);
    }
    protected void addFileButtom_Click(object sender, EventArgs e)
    {
        if (!fileFile.HasFile) return;
        var filename = Path.GetFileName(fileFile.FileName);
       
        fileReport.SaveAs(Server.MapPath("~/") + filename);
        var buffer = new byte[fileFile.PostedFile.InputStream.Length];
        using (var ms = new MemoryStream())
        {
            int read;
            while ((read = fileFile.PostedFile.InputStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            var connectionString = new SqlConnectionStringBuilder()
                {
                    UserID = "sa",
                    Password = "i3",
                    DataSource = "psodata1",
                    InitialCatalog = "ReportStorage"
                };
            using (var conn = new SqlConnection(connectionString.ConnectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand("addReportFile", conn){CommandType = CommandType.StoredProcedure})
                {
                    cmd.Parameters.AddWithValue("@FileBlob", buffer);
                    cmd.Parameters.AddWithValue("@FileType", fileFile.PostedFile.ContentType);
                    cmd.Parameters.AddWithValue("@FileName", filename);
                    cmd.Parameters.AddWithValue("@reportID", reportID);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        Response.Redirect("ViewReport.aspx?" + reportID + "&" + fileID);
    }
   

public class Report
{
    [DataMember]
    public Guid Id { get; private set; }
    
    [DataMember]
    public string Name { get; private set; }

    [DataMember]
    public string Description { get; private set; }

    [DataMember]
    public string OriginalCustomer { get; private set; }

    [DataMember]
    public string CreatedBy { get; private set;}

    [DataMember]
    public string CreatedDate { get; private set; }

    [DataMember]
    public List<Tag> Tags { get; private set; }

    [DataMember]
    public List<ScreenShot> ScreenShots { get; private set; }

    [DataMember]
    public List<ReportFile> Files { get; private set; }

    [DataMember]
    public string Notes { get; private set; }
    
    

    private Report(Guid id, string name, string desc, string orignCust, string createdBy, DateTime createddate)
    {
        Tags = new List<Tag>();
        ScreenShots = new List<ScreenShot>();
        Files = new List<ReportFile>();
        var connectionString = new SqlConnectionStringBuilder()
            {
                UserID = "sa",
                Password = "i3",
                InitialCatalog = "ReportStorage",
                DataSource = "psodata1"
            };
        using (var conn = new SqlConnection(connectionString.ConnectionString))
        {
            conn.Open();
            using (var cmd = new SqlCommand("getReportTags", conn) {CommandType = CommandType.StoredProcedure})
            {
                cmd.Parameters.AddWithValue("@id", id.ToString());
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        this.Tags.Add(new Tag(reader.GetString(0),reader.GetGuid(1)));
                    }
                }
            }
            using (var cmd = new SqlCommand("getScreenShots", conn) { CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.AddWithValue("@Reportid", id.ToString());
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        this.ScreenShots.Add(new ScreenShot(reader.GetGuid(0), null));
                    }
                }
            }
            using (var cmd = new SqlCommand("getReportFiles", conn) { CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.AddWithValue("@reportId", id.ToString());
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        this.Files.Add(new ReportFile(reader.GetGuid(0),reader.GetString(3)));
                    }
                }
            }
            using (var cmd = new SqlCommand("getReportNotes", conn) { CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.AddWithValue("@reportId", id.ToString());
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        this.Notes = reader.GetString(0);
                    }
                }
            }
        }
        Id = id;
        Name = name;
        Description = desc;
        OriginalCustomer = orignCust;
        CreatedBy = createdBy;
        CreatedDate = CreatedDate;
    }

    private Report()
    {
    }

    public static Report GetDataBaseReport(string id)
    {
        var connectionString = new SqlConnectionStringBuilder()
        {
            UserID = "sa",
            Password = "i3",
            InitialCatalog = "ReportStorage",
            DataSource = "psodata1"
        };
        using (var conn = new SqlConnection(connectionString.ConnectionString))
        {
            conn.Open();
            using (var cmd = new SqlCommand("getReport", conn) { CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.AddWithValue("@id", id.ToString());
                using (var reader = cmd.ExecuteReader())
                {
                    
                    while (reader.Read())
                    {
                        return new Report(reader.GetGuid(0),
                                          reader.GetString(1),
                                          reader.GetString(2),
                                          reader.GetString(3),
                                          reader.GetString(4),
                                          reader.GetDateTime(5));

                    }
                }
            }
        }
        return null;
    }


    public static void AddTag(string reportId,string tag)
    {
        if (string.IsNullOrEmpty(reportId) || string.IsNullOrEmpty(tag)) return;
        var connectionString = new SqlConnectionStringBuilder()
        {
            UserID = "sa",
            Password = "i3",
            InitialCatalog = "ReportStorage",
            DataSource = "psodata1"
        };
        using (var conn = new SqlConnection(connectionString.ConnectionString))
        {
            conn.Open();
            using (var cmd = new SqlCommand("addReportTag", conn) { CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.AddWithValue("@reportid", reportId.ToString());
                cmd.Parameters.AddWithValue("@tag", tag);
                cmd.ExecuteNonQuery();
                
            }
        }
    }

    public static void AddScreenShot(string reportId, byte[] sC)
    {
        var connectionString = new SqlConnectionStringBuilder()
        {
            UserID = "sa",
            Password = "i3",
            InitialCatalog = "ReportStorage",
            DataSource = "psodata1"
        };
        using (var conn = new SqlConnection(connectionString.ConnectionString))
        {
            conn.Open();
            using (var cmd = new SqlCommand("addScreenShot", conn) { CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.AddWithValue("@ReportId", reportId.ToString());
                cmd.Parameters.AddWithValue("@pic", sC);
                cmd.ExecuteNonQuery();

            }
        }
    }



    public static string GetJson(Report report)
    {
        var json = new JavaScriptSerializer().Serialize(report);
        return json;
    }
    
}
public class Tag
{
    [DataMember]
    public string Name { get; private set; }

    [DataMember]
    public Guid Id { get; private set; }

    public Tag(string name, Guid id)
    {
        Name = name;
        Id = id;
    }
}

public class ScreenShot
{
    [DataMember]
    public Guid Id { get; private set; }

    public byte[] Image { get; private set; }

    public ScreenShot(Guid id, byte[] image)
    {
        Id = id;
        Image = image;
    }

}
public class ReportFile
{
    [DataMember]
    public Guid Id { get; private set; }

    [DataMember]
    public string Name { get; private set; }

    public ReportFile(Guid id, string name)
    {
        Id = id;
        Name = name;
    }



}protected void btnUpdateNotes_Click(object sender, EventArgs e)
{
    var connectionString = new SqlConnectionStringBuilder()
    {
        UserID = "sa",
        Password = "i3",
        InitialCatalog = "ReportStorage",
        DataSource = "psodata1"
    };
    using (var conn = new SqlConnection(connectionString.ConnectionString))
    {
        conn.Open();
        using (var cmd = new SqlCommand("updateReportNotes", conn) { CommandType = CommandType.StoredProcedure })
        {
            cmd.Parameters.AddWithValue("reportId", reportID);
            cmd.Parameters.AddWithValue("@note", txtNotes.Text);
            cmd.ExecuteNonQuery();
        }
    }
    Response.Redirect("ViewReport.aspx?"+reportID);
}
}
