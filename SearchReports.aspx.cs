﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BrowseReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        try
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        catch(NullReferenceException){}
    }
    protected void txtReportDesc_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SqlDataSource1.SelectCommand = "SELECT DISTINCT r.* FROM [Reports] r" +
                                       " left outer join ReportTagLink rtl" +
                                       " on r.id = rtl.ReportID" +
                                       " left outer join UniqueTags ut" +
                                       " on rtl.TagID = ut.id" +
                                       " left outer join ReportNotes rn" +
                                       " on r.id = rn.ReportId" +
                                       " Where r.Name like '%@p1%'" +
                                       " or r.Description like '%@p1%'" +
                                       " or r.OriginalCustomer like '%@p1%'" +
                                       " or ut.tag like '%@p1%'" +
                                       " or rn.notes like '%@p1%'";
        SqlDataSource1.SelectCommand = SqlDataSource1.SelectCommand.Replace("@p1", txtReportDesc.Text);
    }
}