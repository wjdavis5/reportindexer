﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddReport.aspx.cs" Inherits="AddReport" MasterPageFile="MasterPage.master" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
      <style type="text/css">
          .controls {
              height: 66px;
          }
      </style>
   </asp:Content>
  <asp:Content runat="server"  ContentPlaceHolderID="Body">
      <div class="container theme-showcase">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Report Indexer</h1>
        
            
<fieldset>

<!-- Form Name -->
<legend>Add a Report</legend>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="txtReportName">Report Name</label>
  <div class="controls">
      <asp:TextBox id="txtReportName" name="txtReportName" type="text" placeholder="name" class="input-xlarge" required="true" style="width:100%;" runat="server"/>
  </div>
</div>

<!-- Textarea -->
<div class="control-group">
  <label class="control-label" for="txtReportDesc">Report Description</label>
  <div class="controls">                     
      <asp:TextBox id="txtReportDesc" name="txtReportDesc" type="text" placeholder="name" class="input-xlarge" required="true" style="width:100%;" runat="server"/>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="txtOriginalCust">Original Customer</label>
  <div class="controls">
      <asp:TextBox id="txtOriginalCust" name="txtOriginalCust" type="text" placeholder="customer" class="input-xlarge" style="width:100%;" runat="server"/>
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="txtCreatedBy">Written By</label>
  <div class="controls">
      <asp:TextBox id="txtCreatedBy" name="txtCreatedBy"  type="text" placeholder="WrittenBy" class="input-xlarge" style="width:100%" runat="server" runat="server"/>
    
  </div>
</div>

<!-- File Button --> 
<div class="control-group">
  <label class="control-label" for="fileReport">Upload Report - Additional Files added on a later screen</label>
  <div class="controls">
      <asp:FileUpload id="fileReport" required="true" name="fileReport" class="input-file" type="file" style="width:100%" runat="server"/>
       <label class="control-label" for="fileReport">Upload File Config (xml)</label>
  <div class="controls">
      <asp:FileUpload id="fileConfigUpload" name="fileConfi" class="input-file" type="file" style="width:100%" runat="server"/>
      <br/>
  </div>
</div>
    </div>
    <br/>
<!-- Button (Double) -->
<div class="control-group">
  <label class="control-label" for="btnSubmit">Finish</label>
  <div class="controls">
      <asp:Button id="btnSubmit" name="btnSubmit" class="btn btn-success" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
      
      <asp:button id="btnCancel" name="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" />

  </div>
</div>

</fieldset>


        
        
      </div>
          </div>
</asp:Content>