﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" MasterPageFile="MasterPage.master" %>


 
<asp:content runat="server" ContentPlaceHolderID="Body">
    <div class="container theme-showcase">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Report Indexer</h1>
        <p>Report Indexer is a repository for reports that have been written. We can use this tool to upload reports for safe keeping and also associate tags for easy searching</p>
        <p><a href="AddReport.aspx" class="btn btn-primary btn-lg">Add a Report &raquo;</a></p>
      </div>
        <div id="myCanvasContainer">
  <canvas width="300" height="300" id="myCanvas">
    <p>Anything in here will be replaced on browsers that support the canvas element</p>
  </canvas>
</div>
<div id="tags">
  
</div>
          </div>
</asp:content>
      
