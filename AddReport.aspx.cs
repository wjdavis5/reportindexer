﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class AddReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!fileReport.HasFile) return;

        //removed 12/10 to allow reports sans config file to upload
        //if (!fileConfigUpload.HasFile) return;
        var reportFileName = Path.GetFileName(fileReport.FileName);
        fileReport.SaveAs(Server.MapPath("~/") + reportFileName);
        var reportBuffer = new byte[fileReport.PostedFile.InputStream.Length];
        using (var ms = new MemoryStream())
        {
            int read;
            while ((read = fileReport.PostedFile.InputStream.Read(reportBuffer, 0, reportBuffer.Length)) > 0)
            {
                ms.Write(reportBuffer, 0, read);
            }
        }
        if (fileConfigUpload.HasFile)
        {
            var configFileName = Path.GetFileName(fileReport.FileName);
            fileConfigUpload.SaveAs(Server.MapPath("~/") + configFileName);
            var configBuffer = new byte[fileConfigUpload.PostedFile.InputStream.Length];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = fileConfigUpload.PostedFile.InputStream.Read(configBuffer, 0, configBuffer.Length)) > 0)
                {
                    ms.Write(configBuffer, 0, read);
                }
            }
            InsertReport(txtReportName.Text,
                txtReportDesc.Text,
                txtOriginalCust.Text,
                txtCreatedBy.Text,
                reportBuffer,
                reportFileName,
                fileReport.PostedFile.ContentType,
                configBuffer,
                configFileName,
                fileConfigUpload.PostedFile.ContentType);
            try
            {
                File.Delete(reportFileName);
                File.Delete(configFileName);
            }
            catch (Exception) { }
        }
        else
        {
            InsertReport(txtReportName.Text,
          txtReportDesc.Text,
          txtOriginalCust.Text,
          txtCreatedBy.Text,
          reportBuffer,
          reportFileName,
          fileReport.PostedFile.ContentType,
          new byte[] { },
          string.Empty,
          string.Empty);
            try
            {
                File.Delete(reportFileName);
            }
            catch (Exception) { }
        }
    }

    private void InsertReport(string name, string desc, string originalCust, string createdBy,
        byte[] rFile, string rfileName, string rfileType,
        byte[] cFile, string cfileName, string cfileType)
    {
        string queryString = string.Empty;
        var connectionString = new SqlConnectionStringBuilder()
        {
            ApplicationName = "Report Uploader",
            UserID = "sa",
            Password = "i3",
            DataSource = "psodata1",
            InitialCatalog = "ReportStorage"
        };
        using (var conn = new SqlConnection(connectionString.ConnectionString))
        {
            conn.Open();
            using (var cmd = new SqlCommand("createReport", conn) { CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@Description", desc);
                cmd.Parameters.AddWithValue("@OriginalCustomer", originalCust);
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                cmd.Parameters.AddWithValue("@ReportFile", rFile);
                cmd.Parameters.AddWithValue("@FileType", rfileType);
                cmd.Parameters.AddWithValue("@FileName", rfileName);
                cmd.Parameters.AddWithValue("@configFile", cFile);
                cmd.Parameters.AddWithValue("@cFileType", cfileType);
                cmd.Parameters.AddWithValue("@cFileName", cfileName);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var reportID = reader.GetGuid(0);
                        var fileID = reader.GetGuid(1);
                        queryString = "?" + reportID.ToString() + "&" + fileID.ToString();
                    }
                }

            }
        }
        Response.Redirect("ViewReport.aspx" + queryString);
    }
}