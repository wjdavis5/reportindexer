﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchReports.aspx.cs" Inherits="BrowseReports" MasterPageFile="MasterPage.master" %>
<asp:Content runat="server" ContentPlaceHolderID="Body">
      <div class="container theme-showcase">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Search Reports</h1>
        <p><a href="AddReport.aspx" class="btn btn-primary btn-lg">Add a Report &raquo;</a></p>
          <div class="control-group">
  <label class="control-label" for="txtReportDesc">Report Search - (Tags, Name, Description, Cutsomer)</label>
  <div class="controls">                     
      <asp:TextBox id="txtReportDesc" name="txtReportDesc" type="text" placeholder="name" class="input-xlarge" required="true" style="width:100%;" runat="server" OnTextChanged="txtReportDesc_TextChanged"/>
      <asp:Button runat="server" CssClass="btn-lg btn-success" Text="Search" ID="btnSearch" OnClick="btnSearch_Click"/>
  </div>
</div>
      </div>
        
          </div>
      <div >
              <asp:GridView ID="GridView1" runat="server"
                  CssClass="table table-hover table-striped" GridLines="None" 
                   AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" OnDataBound="GridView1_DataBound">
                  <Columns>
                      <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="ViewReport.aspx?{0}" HeaderText="View Report" Text="View Report" />
                      <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                      <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                      <asp:BoundField DataField="OriginalCustomer" HeaderText="OriginalCustomer" SortExpression="OriginalCustomer" />
                      <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />
                      <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate" />
                  </Columns>
                  <RowStyle CssClass="cursor-pointer" />
                  <EmptyDataTemplate>
                      <h1>
                      <span class="label label-danger">
                      NO REPORTS FOUND!
                          </span>
                          </h1>
                  </EmptyDataTemplate>
              </asp:GridView>
              <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ReportStorageConnectionString %>" SelectCommand="SELECT * FROM [Reports]"></asp:SqlDataSource>
          </div>
    </asp:Content>