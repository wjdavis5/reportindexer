﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewReport.aspx.cs" Inherits="ViewReport" MasterPageFile="MasterPage.master" %>


<asp:Content runat="server" ContentPlaceHolderID="Head">
    
    
      <script type="text/javascript">
          var reportID = '<%= reportID%>';
          var fileID = '<%= fileID%>';
          var report = <%= Report.GetJson(_report)%>
          $(document).ready(function () {
              $("#Body_txtTagName").attr("required", false);
              $("#name").html(report.Name);
              $("#desc").html(report.Description);
              $("#oC").html(report.OriginalCustomer);
              $("#cb").html(report.CreatedBy);
              $("#cd").html(report.CreatedDate);
              $("textarea#Body_txtNotes").val(report.Notes);
              for (var a in report.Tags) {
                  $("#tag-cloud").append("<li class='tag-cloud tag-cloud-info'>" + report.Tags[a].Name + "</li>");
                  if (a % 9 == 0) {
                      $("#tag-cloud").append("<br />");
                  }
              }
              for (var a in report.ScreenShots) {
                  $("#screenShots").append("<a href='Handler/ImageViewer.ashx?Full&" + report.ScreenShots[a].Id + "' class='thumbnail'><img src='Handler/ImageViewer.ashx?Full&" + report.ScreenShots[a].Id + "'></a>");

                  
                  if (a % 4 == 0) {
                      $("#screenShots").append("<br />");
                  }
              }
              for (var a in report.Files) {
                  $("#files").append("<a href='Handler/ReportDownload.ashx?" + report.Files[a].Id + "' class='thumbnail'><img src='Content/IMG/RPTicon.jpg' width='50' height='50'>"+report.Files[a].Name+"</a>");


                  if (a % 4 == 0) {
                      $("#files").append("<br />");
                  }
              }
             
          });
          var showAddTag = function () {
              $("#addTagWindow").fadeIn();
              $("#txtTagName").attr("required", false);
              $("#Body_txtTagName").attr("required", false);
              $("#txtTagName").attr("required", false);
          };
          var showAddScreenShot = function () {
              $("#addScreenShot").fadeIn();
              $("#txtTagName").attr("required", false);
              $("#Body_txtTagName").attr("required", false);
              $("#txtTagName").attr("required", false);
              
          };
          var showAddFile = function () {
              $("#addFile").fadeIn();
              $("#txtTagName").attr("required", false);
              $("#Body_txtTagName").attr("required", false);
              $("#txtTagName").attr("required", false);

          };
          var hideAddFile = function () {
              $("#txtTagName").attr("required", false);
              $("#addFile").fadeOut();
              $("#txtTagName").attr("required", false);
              $("#Body_txtTagName").attr("required", false);
              $("#txtTagName").attr("required", false);
          };
          var hideAddScreenShot = function () {
              $("#addScreenShot").fadeOut();//Body_txtTagName
              $("#Body_txtTagName").attr("required", false);
              $("#txtTagName").attr("required", false);
          };
          var hideAddTag = function () {
              $("#addScreenShot").fadeOut();
              $("#Body_txtTagName").attr("required", false);
              $("#txtTagName").attr("required", false);
          };
      </script>
    </asp:Content>
 <asp:Content runat="server" ContentPlaceHolderID="Body">
      <div class="container theme-showcase">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>View Report <a href="ReportDownload.ashx?"></a></h1>
        <h2> <span class="label label-success" style="width:100%">Report Name:</span></h2><span id="name" class="label label-primary"></span><br/>
          <h2> <span class="label label-success">Report Description:</span></h2><span id="desc" class="label label-primary"></span><br/>
          <h2> <span class="label label-success">Original Customer:</span></h2><span id="oC" class="label label-primary"></span><br/>
          <h2> <span class="label label-success">Created By:</span></h2><span id="cb" class="label label-primary"></span><br/>
          <h2> <span class="label label-success">Created Date:</span></h2><span id="cd" class="label label-primary"></span><br/>
        <div class="panel panel-primary"><div class="panel-heading">
          <h2 class="panel-title">Report Details</h2></div>
         <div class="panel-body" id="Div2">
             <asp:TextBox runat="server" TextMode="MultiLine" ID="txtNotes" CssClass="form-control" Rows="6"></asp:TextBox>
         </div>
         <div class="panel-footer"><asp:Button runat="server" ID="btnUpdateNotes" class="btn btn-primary btn-lg" Text="Update" OnClick="btnUpdateNotes_Click" /></div>
        </div>
          <div class="panel panel-primary"><div class="panel-heading">
              <h2 class="panel-title">Tags</h2></div>
          <div class="panel-body" id="tags">
            <ul id="tag-cloud">
                
            </ul>
          </div>
          <div class="panel-footer"><a href="#" onclick="showAddTag()" class="btn btn-info btn-lg">Add a Tag &raquo;</a></div>
        </div>
          <div class="panel panel-primary"><div class="panel-heading">
              <h2 class="panel-title">ScreenShots</h2></div>
          <div class="panel-body" >
            <div class="row">
  <div class="col-sm-6 col-md-3" id="screenShots">
    
  </div>
  
</div>
          </div>
          <div class="panel-footer"><a href="#" onclick="showAddScreenShot()" class="btn btn-info btn-lg">Add a Picture &raquo;</a></div>
        </div>
           <div class="panel panel-primary"><div class="panel-heading">
              <h2 class="panel-title">Files</h2></div>
          <div class="panel-body" id="files">
            
          </div>
          <div class="panel-footer"><a href="#" onclick="showAddFile()" class="btn btn-info btn-lg">Add a File &raquo;</a></div>
        </div>
          
      </div>
          </div>
     <div id="addTagWindow" style="width:100%;height:100%;opacity: .95;background-color:aliceblue;position: absolute;top:0px;left:0px;margin:0 auto;padding:10% 10%;display: none;">
          <div class="panel panel-primary"><div class="panel-heading">
              <h2 class="panel-title">AddTag</h2></div>
          <div class="panel-body" id="Div1">
            <asp:TextBox id="txtTagName" name="txtTag" type="text" placeholder="name" class="input-xlarge" required="false" style="width:100%;" runat="server"/>
          </div>
          <div class="panel-footer">
              <asp:Button id="btnSubmitTag" name="btnSubmitTag" OnClientClick="hideAddTag()" class="btn btn-info btn-lg" runat="server" Text="Done &raquo;" OnClick="btnSubmitTag_Click" />
              
          </div>
        </div>
      </div>
          
           <div id="addScreenShot" style="width:100%;height:100%;opacity: .95;background-color:aliceblue;position: absolute;top:0px;left:0px;margin:0 auto;padding:10% 10%;display:none;">
          <div class="panel panel-primary"><div class="panel-heading">
              <h2 class="panel-title">Add ScreenShot</h2></div>
          <div class="panel-body" id="Div4">
             <asp:FileUpload id="fileReport" name="fileReport" class="input-file" type="file" style="width:100%" runat="server"/>
          </div>
          <div class="panel-footer">
              <asp:Button id="subSC" name="btnSubmitSC" OnClientClick="hideAddScreenShot()"  class="btn btn-info btn-lg" runat="server" Text="Done &raquo;" OnClick="Button1_Click1" />
              
          </div>
        </div>
      </div>
     
      <div id="addFile" style="width:100%;height:100%;opacity: .95;background-color:aliceblue;position: absolute;top:0px;left:0px;margin:0 auto;padding:10% 10%;display:none;">
          <div class="panel panel-primary"><div class="panel-heading">
              <h2 class="panel-title">Add File</h2></div>
          <div class="panel-body" id="Div3">
             <asp:FileUpload id="fileFile" name="fileFile" class="input-file" type="file" style="width:100%" runat="server"/>
          </div>
          <div class="panel-footer">
              <asp:Button id="addFileButtom" name="btnSubmitSC" OnClientClick="hideAddFile()"  class="btn btn-info btn-lg" runat="server" Text="Done &raquo;" OnClick="addFileButtom_Click" />
              
          </div>
        </div>
      </div>


     </asp:Content>
    